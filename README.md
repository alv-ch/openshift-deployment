# Openshift Deployment

This project is a central location that contains all deployment scripts to deploy the complete Jobroom app on Openshift.

This includes:
* Network policies
* Config maps
* Secrets
* Infrastructure Services (ES, Kafka, Postgres, etc.)
* Services (Gateway, Candidate Service, Portal, etc.)
